# -*- coding: utf-8 -*-
"""
MTMW12 Assignment 3

Kimberley Mirren
28842057
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import dataframe_image as dfi



# Python3 code to numerically differentiate the pressure in order to calculate
# the geostaphic wind relation using 2-point differencing and
# compare with the analytic solution and plot.

# Physical properties and functions for calculating the geostophic wind

# A dictionary of physical properties
physProps = {'pa'  : 1e5,     # The mean pressure
             'pb'  : 200,     # The magnitude of the pressure variations
             'f'   : 1e-4,    # The Coriolis parameter
             'rho' : 1.,      # Density
             'L'   : 2.4e6,   # Length scale of the pressure variations (k) 
             'ymin': 0.,      # Start of the y domain  
             'ymax': 1e6}     # End of the y domain

def pressure(y, props):
    """The pressure and given y locations based on dictionary /
    of physical properties, props"""
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    return  pa + pb*np.cos(y*np.pi/L)

def uGeoExact(y, props):
    """The analytic geostaphic wind at given locations, /
    y based on dictionary of physical properties, props"""
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    """The geostaphic wind as a function of pressure gradient based /
    on dictionary of physical properties, props"""
    rho = props["rho"]
    f = props["f"]
    return -dpdy/(rho*f)

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance /
    dx apart using 2-point differences. Returns an array the same size as f"""
    
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    #Two point differences at the end points
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx
    # Centred differences for the mid-points
    for i in range(1, len(f)-1):
        dfdx[i] = (f[(i+1)]-f[(i-1)])/(2*dx)
    return dfdx

def gradient_4point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance 
    dx apart using 4-point differences. Returns an array the same size as f"""
    
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    #Two point differences at the end points
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[1] = (f[(2)]-f[(0)])/(2*dx)
    dfdx[-1] = (f[-1]-f[-2])/dx
    dfdx[-2] = (f[(-1)]-f[(-3)])/(2*dx)
    # Centred differences for the mid-points
    for i in range(2, len(f)-2):
        dfdx[i] = (-f[(i+2)]+8*f[i+1]-8*f[(i-1)]+f[i-2])/(12*dx)
    return dfdx


def geostrophicWind(N, O):
    """Calculate the geostrophic wind analytically and numerically and plot 
       for N intervals, and order O (NB. O can only be 2 or 4 currently) """
    
    # Resolution and size of the domain
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N        # The length of the spacing

    
    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)

    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using four point differences
    if O == 2:
        dpdy = gradient_2point(p, dy)
    elif O == 4:
        dpdy = gradient_4point(p, dy)
    else:
        print("Only 2nd or 4th order approximations available.")
    
    u_point = geoWind(dpdy, physProps)
    
    # The pressure gradient excluding the end points
    if O == 2:
        uExact_2 = uExact[1:-2]
        u_point_2 = u_point[1:-2]
        y_2 = y[1:-2]
    elif O == 4:
        uExact_2 = uExact[2:-3]
        u_point_2 = u_point[2:-3]
        y_2 = y[2:-3]
    else:
        print("Only 2nd or 4th order approximations available.")   

        
    # Graph to compare the numerical and analytic solutions
    # Plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    #Plot the approximate and exact wind at y points
    #plt.title("Fig 1: Analytical & Numerically Approximated Wind", 
    #          fontsize = 10)
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_point, 'k*--', label = 'Two-point differences', \
        ms = 12, markeredgewidth = 1.5, markerfacecolor='r')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    if O == 2:
        plt.savefig('geoWindCent_Ord2.pdf')
    elif O == 4:
        plt.savefig('geoWindCent_Ord4.pdf')
    else:
        print("Only 2nd or 4th order approximations available.") 
    plt.show()
    
   
    #plt.title("Fig 2: Error Analysis of Analytical & Numerically" 
    #          " Approximated Wind",  fontsize = 10)
    plt.plot(y/1000, uExact - u_point, 'k-', label = 'Errors')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    if O ==2:
        plt.savefig('geoWindCentErrors_Ord2.pdf')
    elif O == 4:
        plt.savefig('geoWindCentErrors_Ord4.pdf')
    else:
        print("Only 2nd or 4th order approximations available.")
    plt.show()
 
    
    # Plot the errors excluding the end points to zone in on the mid-point
    # errors and show that they are not zero
    #plt.title("Fig 3: Further Error Analysis of Numerical Approximation", 
    #          fontsize = 10)
    plt.plot(y_2/1000, uExact_2 - u_point_2, 'k-', label = 'Errors')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    if O ==2:
        plt.savefig('geoWindCentErrors_2_Ord2.pdf')
    elif O == 4:
        plt.savefig('geoWindCentErrors_2_Ord4.pdf')
    plt.show()

geostrophicWind(10, 2)
geostrophicWind(10, 4)
    
def Log_Err(N, O):   
    """Define an array containing resolution (delta y), error (epsilon)
    at mid point of data, & associated logarithms of delta y & epsilon"""
        
    # Resolution and size of the domain
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N        # The length of the spacing

    
    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)

    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using four point differences
    if O == 2:
        dpdy = gradient_2point(p, dy)
    elif O == 4:
        dpdy = gradient_4point(p, dy)
    else:
        print("Only 2nd or 4th order approximations available.")

        
    u_point = geoWind(dpdy, physProps)
 
    
    # The pressure gradient excluding the end points
    if O == 2:
        uExact_2 = uExact[1:-2]
        u_point_2 = u_point[1:-2]
        y_2 = y[1:-2]
    elif O == 4:
        uExact_2 = uExact[2:-3]
        u_point_2 = u_point[2:-3]
        y_2 = y[2:-3]
    else:
        print("Only 2nd or 4th order approximations available.")
    
    # Set an array to contain the log of the error
    # and the log of the resolution
    Error = abs(uExact - u_point)
    arraySize = 4
    N_pt = np.zeros(arraySize)
    # The error has been taken from the mid-point (N/2)
    N_pt[0] = np.log(Error[int(N/2)])
    N_pt[1] = np.log(dy)
    N_pt[2] = dy
    N_pt[3] = Error[int(N/2)]
    return(N_pt)

def Err_Ord(N_1, N_2, O):
    """"Order of convergence for O-th order approximation with two resolutions
    relating to N_1 & N_2 intervals."""
    Order =  (Log_Err(N_1, O)[0] - Log_Err(N_2, O)[0]) \
        / (Log_Err(N_1, O)[1] - Log_Err(N_2, O)[1])
    return Order


def Table(O):
    """"Create table of data to check the order of convergence
        for an O-th order approxiamtion"""
    data = { 'i': [1, 2, 3, 4], \
            'N':[10, 20, 50, 100], \
            'delta y_i':[Log_Err(10,O)[2], Log_Err(20,O)[2], \
                          Log_Err(50,O)[2], Log_Err(100,O)[2]], \
            '\error_i': [Log_Err(10,O)[3], Log_Err(20,O)[3],\
                               Log_Err(50,O)[3], Log_Err(100,O)[3]], \
            'log(delta y_i)':[Log_Err(10,O)[0], Log_Err(20,O)[0], \
                          Log_Err(50,O)[0], Log_Err(100,O)[0]], \
            'log(error_i)': [Log_Err(10,O)[1], Log_Err(20,O)[1],\
                               Log_Err(50,O)[1], Log_Err(100,O)[1]], \
            'n_i': [Err_Ord(10, 20, O), Err_Ord(20, 50,O), \
                     Err_Ord(50, 100, O), "-"] }
        
    plt.rcParams["figure.figsize"] = [7.50, 3.50]
    plt.rcParams["figure.autolayout"] = True
    df = pd.DataFrame(data)
    #df.plot()
    if O == 2:
        dfi.export(df,"Error Convergence Table_2.png")
    elif O == 4:
        dfi.export(df,"Error Convergence Table_4.png")
    else:
        print("Only 2nd or 4th order approximations available.")
    return(df)

Table(2)
Table(4)
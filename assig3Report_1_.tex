\documentclass[12pt]{article}
\usepackage[font={small,it}]{caption}

% compile this latex file with "pdflatex assig3Report"
% Lines staring with % are comments
% Graphics files are included with the \includegraphics command. 
% You will need to un-comment these to use them.

% additional latex packages to use
\usepackage[a4paper, left=2.2cm,top=1.5cm, right=2.2cm,bottom=1.5cm,]{geometry}
\usepackage{times, graphicx, amsmath, mathtools}
\usepackage{url,multirow,xfrac}

% not so much space around floats
\renewcommand{\floatpagefraction}{0.95}
\renewcommand{\textfraction}{0}
\renewcommand{\topfraction}{1}
\renewcommand{\bottomfraction}{1}

\begin{document}
\thispagestyle{empty}

\title{MTMW12, Assignment 3, Numerical Differentiation}
\author{28842057}
\maketitle

The code is at \url{https://gitlab.act.reading.ac.uk/28842057/MTMW12_assig3}.

\begin{enumerate}
\item\label{q:diff} The wind, $u$, is evaluated from the pressure, $p$, by numerically differentiating the geostrophic wind relation:
\begin{equation}
u = -\frac{1}{\rho f}\frac{dp}{dy}
\label{eq:geoWind}
\end{equation}
where the pressure, $p$, is given by:
\begin{equation}
p = p_a + p_b \cos\frac{y\pi}{L}
\label{eq:cosPressure}
\end{equation}
where $p_a=10^5 \text{Pa}$, $p_b=200 \text{Pa}$, $f = 10^{-4}\text{s}^{-1}$, $\rho=1 \text{kg} \text{m}^{-3}$, $L = 2.4\times 10^6 \text{m}$ and $y:0\rightarrow 10^6 \text{m}$. The domain is divided into $N=10$ equal intervals. The exact $u$ is:
\begin{equation}
u_e = \frac{p^\prime\pi}{\rho f L}\sin\frac{y\pi}{L}.
\label{eq:exactU}
\end{equation}
The wind is evaluated numerically from the pressure gradient using the centred, second-order, two-point finite difference formula away from the end-points of the domain and using one-sided, first-order approximations at the end points to calculate the pressure gradients. The exact and numerically evaluated winds are shown in \textit{Figure \ref{fig: f1}} on the left and the errors between them are shown in \textit{Figure \ref{fig: f2}} on the right. Code for generating these figures is in file \url{MTMW12_Assignment_3_1.py}.

\begin{figure}[htb]
\centering
    \begin{minipage}{0.475\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{geoWindCent_Ord2.pdf} 
        \caption[8pt]{The analytic wind from the geostrophic relation and the numerical approximation calculated using two-point differences.}
	\label{fig: f1}
    \end{minipage}\hfill
    \begin{minipage}{0.475\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{geoWindCentErrors_Ord2.pdf} 
        \caption[8pt]{The errors between the analytic wind from the geostrophic relation and the numerical approximation.}
	\label{fig: f2}
    \end{minipage}
\end{figure}

\textit{Figure  \ref{fig: f1}} shows that the numerical approximation is an accurate method. There is little difference between the graphs except at $y_0$ and $y_{10}$ where the forward and backward differences have been used respectively. This is further indicated in the error analysis in \textit{Figure \ref{fig: f2}}. However, due to the scale of the graph it is difficult to identify that the errors are not in fact zero.

\textit{Figure \ref{fig: f3}} excludes the end points and zooms in on the portion of the graph where the centred difference has been used, showing the actual errors between points $y_1$ and $y_9$ are indeed small, but not zero.

\begin{figure}[htb]
        \includegraphics[width=0.9\textwidth]{geoWindCentErrors_2_Ord2.pdf} 
        \caption{The errors between points $y_1$ and $y_9$.}
	\label{fig: f3}
\end{figure}

Having used the centred second order finite difference formula to approximate $\frac{dp}{dy}$, we would expect the approximation to be second order accurate and would therefore expect the error, $\varepsilon$, to be proportional to ${\Delta}{y^2}$.

Using the order of convergence formulae we would expect to attain a value of two to signify that the approximation is second order accurate.

\begin{equation}
n = \frac{log(\varepsilon_1) - log(\varepsilon_2)}{log({\Delta}{y_1}) - log({\Delta}{y_2})}
\end{equation}

The results are summarised in \textit{Figure \ref{fig: f4}}.

\begin{figure}[htb]
        \includegraphics[width=0.9\textwidth]{Error Convergence Table_2.png} 
        \caption{The order or error ${n_i}$.}
	\label{fig: f4}
\end{figure}

The resolution, ${\Delta}{y_i}$, has been calculated using ${N}$ intervals, and the associated error, ${\varepsilon}_i$ has been taken as the error at the mid point interval $\frac{N}{2}$. For simplicity we have set $n = n_i$ where, 

\begin{equation}
n_i = \frac{log(\varepsilon_i) - log(\varepsilon_{i+1})}{log({\Delta}{y_i}) - log({\Delta}{y_{i+1}})}
\end{equation}

but we could just as well have used the difference between any pair of $\varepsilon_i$ and ${\Delta}{y}$ respectively.

From the table we can clearly see that $n_i \approx 2$, therefore confirming that the second order approximation is indeed second order accurate.

The code for generating these results is in file  \url{MTMW12_Assignment_3_1.py}.

To improve the accuracy of the numerical differentiation we can use a higher-order method. The following fourth order finite difference formula should be fourth order accurate and we would therefore expect the error, $\varepsilon$, to be proportional to ${\Delta}{y^4}$.

\begin{equation}
\frac{dp}{dy}(y_j) = {p^\prime}_{j} = \frac{-p_{j+2} + 8p_{i+1} - 8p_{i-1} + p_{i-2}}{12\Delta y}
\end{equation}

The exact and numerically evaluated winds using equation (6) are shown in \textit{Figure \ref{fig: f6}} on the left and the errors between them are shown in \textit{Figure \ref{fig: f7}} on the right.

\begin{figure}[htb]
\centering
    \begin{minipage}{0.475\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{geoWindCent_Ord4.pdf} 
        \caption[8pt]{The analytic wind from the geostrophic relation and the numerical approximation calculated using two-point differences.}
	\label{fig: f6}
    \end{minipage}\hfill
    \begin{minipage}{0.475\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{geoWindCentErrors_Ord4.pdf} 
        \caption[8pt]{The errors between the analytic wind from the geostrophic relation and the numerical approximation.}
	\label{fig: f7}
    \end{minipage}
\end{figure}

Again, as expected, the graphs show that the errors are small. We have a similar problem with the end points where we can only use forward and backward differences which therefore increases the errors at these points. Further, we have had to make use of the second order centred difference formula for the points inward of the end points. The graphs show that the errors at these points are also larger.

\textit{Figure \ref{fig: f8}} shows the errors between points $y_2$ and $y_8$, where only the fourth order approximation has been used. As is evident, the errors have significantly decreased from the second order approximation.
\begin{figure}[htb]
        \includegraphics[width=0.9\textwidth]{geoWindCentErrors_2_Ord4.pdf} 
        \caption{The errors between points $y_2$ and $y_8$.}
	\label{fig: f8}
\end{figure}

As before, we can use the order of convergence formula (4) to check that the approximation is fourth order accurate. Again, we will set $n = n_i$ where $n_i$ is defined in equation (5). The results are shown below in \textit{Figure \ref{fig: f9}}.
\begin{figure}[ht]
        \includegraphics[width=0.9\textwidth]{Error Convergence Table_4.png} 
        \caption{The order or error ${n_i}$.}
	\label{fig: f9}
\end{figure}
From the table we can clearly see that $n_i \approx 4$, therefore confirming that the fourth order approximation is indeed fourth order accurate.

\end{enumerate}
\end{document}

